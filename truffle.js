// truffle migrate --reset (--network=local)
module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 9545,
      network_id: "*",
    },
    developmentStable: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      // gas : 2500000,
      // gasPrice : 1,
			// from : "0x5aeda56215b167893e80b4fe645ba6d5bab767de",
    }
  }
};
