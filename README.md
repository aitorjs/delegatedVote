# Smart contract and dapp example.

Delegated voted.

# Deploy to memory blockchain (truffle)
    - truffle develop
    - truffle migrate --reset (--network=local)



# Trash

Using network 'development'.

Running migration: 1_initial_migration.js
  Replacing Migrations...
  ... 0x9373e819e9e4419ab3761b6b01b94905c364b337214b407e63d64a62b2eb27f4
  Migrations: 0x8cdaf0cd259887258bc13a92c0a6da92698644c0
  Replacing DelegatedVote...
  ... 0xefc5eb34da6b530c28ef3ff943a1b79df26238603acd398f86c9b1d3639b10e3
  DelegatedVote: 0xf12b5dd4ead5f743c6baa640b0216200e89b60da
Saving successful migration to network...
  ... 0x57b1b513cedfb4bd94fedf949f8c494435abb4e0b43a5b9fadeae23f5bc83256
Saving artifacts...

> truffle console

> var delegatedVote = null;

> DelegatedVote.deployed('0x627306090abab3a6e1400e9345bc60c78a8bef57').then(function(instance) { delegatedVote = instance;});
> delegatedVote.chairperson(): Saber la direccion del presidente.
> '0x627306090abab3a6e1400e9345bc60c78a8bef57'

> delegatedVote.giveRightToVote(0x6330a553fc93768f612722bb8c2ec78ac90b3bbc).then(console.log): Dar derecho a voto
> delegatedVote.getVotersWithRights.call().then(console.log): Sacar votantes con derecho


> delegatedVote.getChairPerson().then(console.log)
> { tx: '0x3f8aed1b92c89a939c2760d8e1a32fc880fa694cf46675781d158a7b3707ce5c',
>  receipt: 
>   { transactionHash: '0x3f8aed1b92c89a939c2760d8e1a32fc880fa694cf46675781d158a7b3707ce5c',
>     transactionIndex: 0,
>     blockHash: '0x7e5fb6c8cb4bea0d67683d0948b691d96d3862067f2cb0d4f46f41cab5d18d54',
>     blockNumber: 4,
>     gasUsed: 21846,
>     cumulativeGasUsed: 21846,
>     contractAddress: null,
>     logs: [],
>     status: 1 },
>  logs: [] }
