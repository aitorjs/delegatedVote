var Migrations = artifacts.require("./Migrations.sol");
var DelegatedVote = artifacts.require("./DelegatedVote.sol");

module.exports = function(deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(DelegatedVote);
};
